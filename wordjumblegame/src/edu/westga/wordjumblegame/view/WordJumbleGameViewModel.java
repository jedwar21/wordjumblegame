/**
 * 
 */
package edu.westga.wordjumblegame.view;

import edu.westga.wordjumblegame.controllers.EasyWordModeController;
import edu.westga.wordjumblegame.controllers.GameController;
import edu.westga.wordjumblegame.controllers.GameMode;
import edu.westga.wordjumblegame.controllers.HardWordModeController;
import edu.westga.wordjumblegame.controllers.RandomWordModeController;
import edu.westga.wordjumblegame.controllers.TwoWordsModeController;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * @author Jessica
 *
 */
public class WordJumbleGameViewModel {

	private final WordJumbleGameGuiCodeBehind codeBehind;
	
	private GameController theGameController;
	
	private RandomWordModeController randomModeController;
	private EasyWordModeController easyModeController;
	private HardWordModeController hardModeController;
	private TwoWordsModeController twoWordsModeController;

	private StringProperty playerScore;
	private StringProperty unscrambledWordInput;
	private StringProperty scrambledWordLabel;
	
	/**
	 * Creates a new ViewModel instance.
	 * 
	 * @precondition none
	 * @postcondition the object and its properties exist
	 */
	public WordJumbleGameViewModel() {
		this.codeBehind = new WordJumbleGameGuiCodeBehind();
		
		this.theGameController = new GameController(this.whichMode());
		
		this.easyModeController = new EasyWordModeController();
		this.hardModeController = new HardWordModeController();
		this.randomModeController = new RandomWordModeController();
		this.twoWordsModeController = new TwoWordsModeController();
		
		this.playerScore = new SimpleStringProperty("0");
	}

	private GameMode whichMode() {
		// TODO Auto-generated method stub
		if (this.codeBehind.hardGameModeAction() == true) {
			return this.hardModeController;
		}
		if (this.codeBehind.easyGameModeAction() == true) {
			return this.easyModeController;
		}
		if (this.codeBehind.twoWordsGameModeAction()== true) {
			return this.twoWordsModeController;
		}
		
		return this.randomModeController;
	}

	public Property<String> playersScoreProperty() {
		// TODO Auto-generated method stub
		return this.playerScore;
	}

	public Property<String> unscrambledWordInputProperty() {
		// TODO Auto-generated method stub
		return this.unscrambledWordInput;
	}

	public Property<String> scrambledWordLabelProperty() {
		// TODO Auto-generated method stub
		return this.scrambledWordLabel;
	}


	
}
