/**
 * 
 */
package edu.westga.wordjumblegame.view;

import edu.westga.wordjumblegame.view.WordJumbleGameViewModel;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;

/**
 * Defines the JavaFX "controller" for the GUI.
 * 
 * @author Jessica Edwards
 * @version Spring, 2015
 *
 */
public class WordJumbleGameGuiCodeBehind {

	private WordJumbleGameViewModel theViewModel;
	
	

	@FXML
	private TextField playersScoreTextField;

	@FXML
	private TextField unscrambleWordInputTextField;
	
	@FXML
	private Label scrambledWordLabel;

	@FXML
	private Menu gameModeMenuBarItem;
	
	@FXML
    private MenuItem exitMenuItem;

	@FXML
	private MenuItem hardGameModeMenuItem;

	@FXML
	private MenuItem easyGameModeMenuItem;

	@FXML
	private MenuItem twoWordsGameModeMenuItem;

	@FXML
	private MenuItem randomWordGameModeMenuItem;

	/**
	 * Creates a new GuiCodeBehind instance.
	 * 
	 * @precondition none
	 * @postcondition the object and its view model exist
	 */
	public WordJumbleGameGuiCodeBehind() {
		this.theViewModel = new WordJumbleGameViewModel();
	}

	/**
	 * Initializes the GUI components, binding them to the view model properties
	 * and setting their event handlers.
	 */
	@FXML
	public void initialize() {
		this.bindGuiComponentsToViewModel();
		this.setEventActions();
	}

	private void setEventActions() {
		this.easyGameModeMenuItem.setOnAction(event -> this.easyGameModeAction());
		this.hardGameModeMenuItem.setOnAction(event -> this.hardGameModeAction());
		this.twoWordsGameModeMenuItem.setOnAction(event -> this.twoWordsGameModeAction());
		this.randomWordGameModeMenuItem.setOnAction(event -> this.randomWordGameModeAction());
		this.exitMenuItem.setOnAction(event -> Platform.exit());

	}

	public boolean randomWordGameModeAction() {
		return true;
	}

	public boolean twoWordsGameModeAction() {
		return true;
	}

	public boolean hardGameModeAction() {
		return true;
	}

	public boolean easyGameModeAction() {
		return true;
	}

	private void bindGuiComponentsToViewModel() {
		this.playersScoreTextField.textProperty().bindBidirectional(
				this.theViewModel.playersScoreProperty());
		this.unscrambleWordInputTextField.textProperty().bindBidirectional(
				this.theViewModel.unscrambledWordInputProperty());
		this.scrambledWordLabel.textProperty().bindBidirectional(this.theViewModel.scrambledWordLabelProperty());
	}
}
