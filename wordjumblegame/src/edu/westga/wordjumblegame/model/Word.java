/**
 * 
 */
package edu.westga.wordjumblegame.model;

import java.io.IOException;

/**
 * This class will model a word in the class
 * 
 * @author Jessica Edwards
 * 
 * @version Spring, 2015
 *
 */
public class Word {
    private String word;

    /**This will create the word
     *
     * @param word              The word to be scrambled
     * 
     * @precondition            name != null
     *
     * @postcondition           The word will be created
     * 
     */
    public Word(String word) throws IOException {
        if (word == null) {
            throw new IllegalArgumentException("Invalid word.");
        }
        
        this.word = word;
    }

    /**Returns the student's name
     * @precondition    none
     * @return          the name of the student
     */
    public String getWord() {
        return this.word;
    }


}
