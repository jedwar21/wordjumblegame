/**
 * 
 */
package edu.westga.wordjumblegame.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**This DataMap will keep track of scrambled words using the unscrambled word as the Key
 * 
 * @author Jessica Edwards
 *
 * @version Spring, 2015
 */

public class WordDataMap implements DataMap<String, Word> {

    private Map<String, Word> theMap;
    
    /**The will create the DataMap
     *
     * @precondition    none
     *
     * @postcondition   The DataMap will be created
     *
     */
    public WordDataMap() {
        this.theMap = new HashMap<>();
    }
    
    /**Adds the value with the specified key to this map.
     * 
     * @param aKey      the key to add
     * @param aValue    the object with the specified key
     * 
     * @precondition    aKey != null && aValue != null
     *                  && !contains(aKey)
     * @postcondition   contains(aKey) && size() == size()@prev + 1
     * 
     */
    @Override
    public void add(String aKey, Word aValue) {
        if (aKey == null) {
            throw new IllegalArgumentException("The key was null");
        }
        if (aValue == null) {
            throw new IllegalArgumentException("The value was null");
        }
        if (this.contains(aKey)) {
            throw new IllegalArgumentException("Key value is already in the map");
        }
        
        this.theMap.put(aKey, aValue);      
        
    }

    /**
     * Returns the value associated with the specified key.
     * 
     * @precondition    aKey != null
     * 
     * @param aKey      the key to access
     * @return          the value
     */
    @Override
    public Word get(String aKey) {
        if (aKey == null) {
            throw new IllegalArgumentException("The key was null");
        }
        if (!this.contains(aKey)) {
            throw new IllegalArgumentException("Key value is not in the map");
        }
        
        return this.theMap.get(aKey);
    }

    /**
     * Returns true iff this map contains an object with
     * the specified key.
     * 
     * @precondition    aKey != null
     * @param aKey      the key to check for
     * @return          true if the key is found, false otherwise
     */
    @Override
    public boolean contains(String aKey) {
        if (aKey == null) {
            throw new IllegalArgumentException("The key was null");
        }
        
        return this.theMap.containsKey(aKey);
    }

    /**
     * Returns the size of this map.
     * 
     * @precondition    none
     * @return          how many items this map contains
     */
    @Override
    public int size() {
        return this.theMap.size();
    }
    
    /** 
     * Returns a list of the values stored in this map.
     * 
     * @precondition    none
     * @return          the values from the map
     */
    @Override
    public List<Word> values() {
        return new ArrayList<Word>(this.theMap.values());
    }

    /** 
     * Returns a list of the keys of the values stored in this map.
     * 
     * @precondition    none
     * @return          the keys from the map
     */
    @Override
    public List<String> keys() {
        return new ArrayList<String>(this.theMap.keySet());
    }
    
    /**Removes the value with the specified key to this map.
     * 
     * @param aKey      the key to remove
     * 
     * @precondition    aKey != null && aValue != null
     *                  && contains(aKey)
     * @postcondition   !contains(aKey) && size() == size()@prev - 1
     * 
     */
    @Override
    public void remove(String aKey) {
        if (aKey == null) {
            throw new IllegalArgumentException("The key was null");
        }
        if (!this.contains(aKey)) {
            throw new IllegalArgumentException("Key value is not in the map");
        }
        
        this.theMap.remove(this.theMap.get(aKey));
        
        this.theMap.values().remove(this.theMap.get(aKey));
        this.theMap.keySet().remove(aKey);
        
    }

    
    
    

}
